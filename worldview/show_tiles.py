# do okna zobrazi dlazdice
import gi
gi.require_version('Gtk', '3.0')
from gi.repository import Gtk, Gdk
import cairo
import os.path, mapnik


WORLD_FILE = '../assets/TM_WORLD_BORDERS-0.3.shp'
TILE_SIZE = 128

__map = None

class Window(Gtk.Window):
	'The application window implementation.'
	def __init__(self):
		Gtk.Window.__init__(self)
		self._level = 2  # 2 means 2^2 tiles
		self.init_ui()

	def init_ui(self):
		darea = Gtk.DrawingArea()
		darea.connect('draw', self.on_draw)
		self.add(darea)

		self.set_title('show tiles')
		self.resize(800, 600)
		self.connect('delete-event', Gtk.main_quit)

		self.show_all()

	def on_draw(self, wid, cr):
		num_tiles = 2**self._level
		for x in range(num_tiles):
			for y in range(num_tiles):
				self._draw_tile(cr, get_tile_name(self._level, x, y), x*TILE_SIZE, y*TILE_SIZE)

	def _draw_tile(self, cr, tile_path, x, y):
		img = cairo.ImageSurface.create_from_png(tile_path)
		cr.set_source_surface(img, x, y)
		cr.paint()


def init_map(level):
	# symbolizers
	polygon_symb = mapnik.PolygonSymbolizer(mapnik.Color('darkgreen'))

	# rules
	polygon_rule = mapnik.Rule()
	polygon_rule.symbols.append(polygon_symb)

	# styles
	polygon_style = mapnik.Style()
	polygon_style.rules.append(polygon_rule)

	# layers
	map_layer = mapnik.Layer('map_layer')
	map_layer.datasource = mapnik.Shapefile(file=WORLD_FILE)
	map_layer.styles.append('map_style')

	# map
	num_tiles = 2**level
	w = num_tiles*TILE_SIZE
	h = num_tiles*TILE_SIZE
	map = mapnik.Map(w, h)
	map.background = mapnik.Color('steelblue')
	map.append_style('map_style', polygon_style)
	map.layers.append(map_layer)
	map.zoom_all()

	return map


def get_tile_name(level, x, y):
	global __map

	if __map is None:
		__map = init_map(level)

	tile_dir = 'tiles/%d/%d' % (level, x)
	tile_name = os.path.join(tile_dir, '%d.png' % (y, ))

	if not os.path.exists(tile_name):
		if not os.path.exists(tile_dir):
			os.makedirs(tile_dir)

		mapnik.render_tile_to_file(__map, x * TILE_SIZE, y * TILE_SIZE, TILE_SIZE,
			TILE_SIZE, tile_name, 'png')

	return tile_name


def main():
	app = Window()
	Gtk.main()

if __name__ == '__main__':
	main()
