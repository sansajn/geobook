# obalka geometrie
import osgeo.ogr

WORLD_FILE = '../assets/world_borders_0.3/TM_WORLD_BORDERS-0.3.shp'

shapefile = osgeo.ogr.Open(WORLD_FILE)
layer = shapefile.GetLayer(0)

print('layer #0 features: %d' % (layer.GetFeatureCount()))

feature = layer.GetFeature(0)
geom = feature.GetGeometryRef()
envelope = geom.GetEnvelope()

print(type(envelope), 
	'(min_x:%f, max_x:%f, min_y:%f, max_y:%f)' % envelope)
