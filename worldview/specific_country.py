# render scecified country map from shapefile
import mapnik
import osgeo.ogr

WORLD_FILE = '../assets/TM_WORLD_BORDERS-0.3.shp'
WIDTH = 800
HEIGHT = 600
COUNTRY = 'Slovakia'

def ogr_open_shapefile(name):
	return osgeo.ogr.Open(name)

def country_bounds(shapefile_ogr, country):
	'vrati stvoricu (min_x, max_x, min_y, max_y)'
	layer = shapefile_ogr.GetLayer(0)
	for i in range(layer.GetFeatureCount()):
		feature = layer.GetFeature(i)
		country_name = feature.GetField('NAME')
		if country == country_name:
			geom = feature.GetGeometryRef()
			return geom.GetEnvelope()

def main():
	# symbolizers
	polygon_symb = mapnik.PolygonSymbolizer(mapnik.Color('darkgreen'))
	line_symb = mapnik.LineSymbolizer(mapnik.Color('black'), 1)

	# rules
	polygon_rule = mapnik.Rule()
	polygon_rule.symbols.append(polygon_symb)
	line_rule = mapnik.Rule()
	line_rule.symbols.append(line_symb)

	# styles
	country_style = mapnik.Style()
	country_style.rules.append(polygon_rule)
	country_style.rules.append(line_rule)

	# layers
	map_layer = mapnik.Layer('map_layer')
	map_layer.datasource = mapnik.Shapefile(file=WORLD_FILE)
	map_layer.styles.append('map_style')

	# map
	map = mapnik.Map(WIDTH, HEIGHT)
	map.background = mapnik.Color('steelblue')
	map.append_style('map_style', country_style)
	map.layers.append(map_layer)

	ogr_shapefile = ogr_open_shapefile(WORLD_FILE)
	bounds = country_bounds(ogr_shapefile, COUNTRY)

	map.zoom_to_box(mapnik.Envelope(bounds[0], bounds[2], bounds[1], bounds[3]))

	mapnik.render_to_file(map, 'country.png', 'png')

	print('scale_denominator:%f, scale:%f' % (map.scale_denominator(), map.scale()))

if __name__ == '__main__':
	main()
