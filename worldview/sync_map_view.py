# do okna zobrazi dlazdice
import gi
gi.require_version('Gtk', '3.0')
from gi.repository import Gtk, Gdk
import cairo
import mapnik
import shapely.geometry
import os.path
from vector import vec2


WORLD_FILE = '../assets/TM_WORLD_BORDERS-0.3.shp'
TILE_SIZE = 128
MAX_MAP_LEVEL = 10
MIN_MAP_LEVEL = 2


class map_view_window(Gtk.Window):
	'The application window implementation.'
	def __init__(self):
		Gtk.Window.__init__(self)
		self._level = 4  # 4 means 2^4 tiles
		self._origin = vec2((800 - (2**self._level * TILE_SIZE))/2, (600 - (2**self._level * TILE_SIZE))/2)
		self._move_start_point = vec2(0, 0)
		self._darea = None  # map draw area
		self._tiles = tile_provider()
		self._view_w, self._view_h = (0, 0)
		self.init_ui()

	def init_ui(self):
		darea = Gtk.DrawingArea()
		darea.set_events(Gdk.EventMask.BUTTON_PRESS_MASK|Gdk.EventMask.POINTER_MOTION_MASK)
		darea.connect('draw', self.on_draw)
		darea.connect('button-press-event', self.on_button_press)
		darea.connect('motion-notify-event', self.on_motion_notify)
		darea.connect('size-allocate', self.on_size_allocate)
		self.add(darea)
		self._darea = darea

		self.set_events(Gdk.EventMask.KEY_PRESS_MASK)
		self.connect('key-press-event', self.on_key_press)
		self._set_title()
		self.resize(800, 600)
		self.connect('delete-event', Gtk.main_quit)

		self.show_all()

	def on_draw(self, wid, cr):
		cr.translate(self._origin.x, self._origin.y)
		num_tiles = 2**self._level
		ignored_tiles = 0
		for x in range(num_tiles):
			for y in range(num_tiles):
				if self._is_tile_on_screen(self._level, x, y):
					self._draw_tile(cr, self._tiles.get(self._level, x, y), x*TILE_SIZE, y*TILE_SIZE)
				else:
					ignored_tiles += 1
		print('view:' + str((self._view_w, self._view_h)))
		print('origin:' + str(self._origin))
		print('%g%% (%d) ignored tiles' % (float(ignored_tiles)/float(num_tiles**2)*100.0, ignored_tiles))

	def on_motion_notify(self, widget, event):
		''' widget:Gdk.Widget, event:Gdk.EventMotion '''
		if event.state & Gdk.ModifierType.BUTTON3_MASK:  # right mouse button
			pos = vec2(event.x, event.y)
			dist = pos - self._move_start_point
			self._move_map(dist)
			self._move_start_point = pos
			print('on_motion_notify: map moved about ' + str(dist))

	def on_button_press(self, widget, event):
		''' widget:Gtk.Widget, event:Gdk.ButtonEvent '''
		if event.type == Gdk.EventType.BUTTON_PRESS:
			if event.button == 1:  # left mouse button:
				print('left mouse pressed')
			elif event.button == 3:  # right mouse button
				self._move_start_point = vec2(event.x, event.y)
				print('right mouse pressed')

	def on_key_press(self, widget, event):
		'''widget:Gdk.Widget, event:Gdk.EventKey'''
		if event.type == Gdk.EventType.KEY_PRESS:
			if event.string == '+':
				self.zoom_in()
			elif event.string == '-':
				self.zoom_out()

			print('key %d pressed -> %s' % (event.keyval, event.string))

	def on_size_allocate(self, widget, allocation):
		''' widget:Gdk.Widget, allocation:Gdk.Rectangle '''
		self._view_w, self._view_h = (allocation.width, allocation.height)

	def _draw_tile(self, cr, tile_path, x, y):
		img = cairo.ImageSurface.create_from_png(tile_path)
		cr.set_source_surface(img, x, y)
		cr.paint()

	def _move_map(self, dist):
		'\param dist distance vector'
		self._origin = self._origin + dist
		self._origin = vec2(
			clamp(self._origin.x, -self.map_size()+self.view_size()[0], 0),
			clamp(self._origin.y, -self.map_size()+self.view_size()[1], 0))
		print('origin:' + str(self._origin))
		self.queue_map_draw()

	def _is_tile_on_screen(self, level, x, y):
		xoff, yoff = (self._origin.x, self._origin.y)
		w, h = (self._view_w, self._view_h)
		view_rect = shapely.geometry.Polygon([
			(-xoff, h - yoff),
			(w - xoff, h - yoff),
			(w - xoff, -yoff),
			(-xoff, -yoff),
			(-xoff, h - yoff)
		])

		tile_rect = shapely.geometry.Polygon([
			(x * TILE_SIZE, y * TILE_SIZE + TILE_SIZE),
			(x * TILE_SIZE + TILE_SIZE, y * TILE_SIZE + TILE_SIZE),
			(x * TILE_SIZE + TILE_SIZE, y * TILE_SIZE),
			(x * TILE_SIZE, y * TILE_SIZE),
			(x * TILE_SIZE, y * TILE_SIZE + TILE_SIZE)
		])

		return view_rect.intersects(tile_rect)  # TODO: rectangle class will be much more effective

	def _set_title(self):
		self.set_title('show tiles, level:%d' % (self._level, ))

	def queue_map_draw(self):
		self._darea.queue_draw()

	def zoom_in(self):
		if self._level+1 > MAX_MAP_LEVEL:
			print('max zoom level %d already reached' % MAX_MAP_LEVEL)
		else:
			self.set_zoom(self._level+1)

	def zoom_out(self):
		if self._level-1 < MIN_MAP_LEVEL:
			print('min zoom level %d already reached' % MIN_MAP_LEVEL)
		else:
			self.set_zoom(self._level-1)

	def set_zoom(self, level):
		level = clamp(level, MIN_MAP_LEVEL, MAX_MAP_LEVEL)
		if level == self._level:
			return

		rel_pos = self.relative_center_position()
		self._level = level
		self._origin = vec2(
			rel_pos[0] * self.map_size() + self.view_size()[0] / 2.0,
			rel_pos[1] * self.map_size() + self.view_size()[1] / 2.0)

		self._set_title()

		self.queue_map_draw()

	def map_size(self):
		return 2**self._level * TILE_SIZE

	def view_size(self):
		return (800, 600)  # TODO: implement properly

	def relative_center_position(self):
		map_size = self.map_size()
		view_size = self.view_size()
		return (
			(self._origin.x - view_size[0]/2.0) / map_size,
			(self._origin.y - view_size[1]/2.0) / map_size)



# math module

def clamp(x, a, b):
	if x < a:
		return a
	elif x > b:
		return b
	else:
		return x

def clamp_vec2(x, a, b):
	return (clamp(x[0], a[0], b[0]), clamp(x[1], a[1], b[1]))


# tile module
class tile_provider:
	'provides map tiles'
	def __init__(self):
		self._maps = {}

	def get(self, level, x, y):
		if not level in self._maps:
			self._init_map(level)

		map = self._maps[level]

		tile_dir = 'tiles/%d/%d' % (level, x)
		tile_name = os.path.join(tile_dir, '%d.png' % (y,))

		if not os.path.exists(tile_name):
			if not os.path.exists(tile_dir):
				os.makedirs(tile_dir)

			mapnik.render_tile_to_file(map, x * TILE_SIZE, y * TILE_SIZE, TILE_SIZE,
				TILE_SIZE, tile_name, 'png')

			print("tile '" + tile_name + "' created")

		return tile_name


	def _init_map(self, level):
		# symbolizers
		polygon_symb = mapnik.PolygonSymbolizer(mapnik.Color('darkgreen'))
		line_symb = mapnik.LineSymbolizer(mapnik.Color('black'), 1)

		# rules
		polygon_rule = mapnik.Rule()
		polygon_rule.symbols.append(polygon_symb)
		line_rule = mapnik.Rule()
		line_rule.symbols.append(line_symb)

		# styles
		polygon_style = mapnik.Style()
		polygon_style.rules.append(polygon_rule)
		polygon_style.rules.append(line_rule)

		# layers
		map_layer = mapnik.Layer('map_layer')
		map_layer.datasource = mapnik.Shapefile(file=WORLD_FILE)
		map_layer.styles.append('map_style')

		# map
		num_tiles = 2 ** level
		w = num_tiles * TILE_SIZE
		h = num_tiles * TILE_SIZE
		map = mapnik.Map(w, h)
		map.background = mapnik.Color('steelblue')
		map.append_style('map_style', polygon_style)
		map.layers.append(map_layer)
		map.zoom_all()

		self._maps[level] = map


def main():
	app = map_view_window()
	Gtk.main()

if __name__ == '__main__':
	main()
