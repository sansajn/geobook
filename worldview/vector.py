

class vec2:
	def __init__(self, x = 0, y = 0):
		self.x = x
		self.y = y

	def __getitem__(self, idx):
		if idx == 0:
			return self.x
		elif idx == 1:
			return self.y

	def __add__(self, other):
		return vec2(self.x + other.x, self.y + other.y)

	def __sub__(self, other):
		return vec2(self.x - other.x, self.y - other.y)

	def __mul__(self, s):
		return vec2(self.x * s, self.y * s)

	def __div__(self, s):
		return vec2(self.x / s, self.y / s)

	def __str__(self):
		return '(' + str(self.x) + ', ' + str(self.y) + ')'



if __name__ == '__main__':
	v = vec2(2,3)
	v = v * 4
	print(v)
	print(v[0], v[1])
