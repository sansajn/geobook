# render entire map from shapefile
import mapnik

WORLD_FILE = '../assets/world_borders_0.3/TM_WORLD_BORDERS-0.3.shp'
WIDTH = 800
HEIGHT = 600

# symbolizers
polygon_symb = mapnik.PolygonSymbolizer(mapnik.Color('darkgreen'))

# rules
polygon_rule = mapnik.Rule()
polygon_rule.symbols.append(polygon_symb)

# styles
polygon_style = mapnik.Style()
polygon_style.rules.append(polygon_rule)

# layers
map_layer = mapnik.Layer('map_layer')
map_layer.datasource = mapnik.Shapefile(file=WORLD_FILE)
map_layer.styles.append('map_style')

# map
map = mapnik.Map(WIDTH, HEIGHT)
map.background = mapnik.Color('steelblue')
map.append_style('map_style', polygon_style)
map.layers.append(map_layer)
map.zoom_all()

mapnik.render_to_file(map, 'map.png', 'png')

print('scale_denominator:%f, scale:%f' % (map.scale_denominator(), map.scale()))
