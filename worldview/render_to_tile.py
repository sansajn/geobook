# render entire shapefile to tiles
import mapnik

WORLD_FILE = '../assets/TM_WORLD_BORDERS-0.3.shp'
TILE_SIZE = 128
NUM_TILES_X = 4
NUM_TILES_Y = 4
WIDTH = NUM_TILES_X * TILE_SIZE
HEIGHT = NUM_TILES_Y * TILE_SIZE

def main():
	# symbolizers
	polygon_symb = mapnik.PolygonSymbolizer(mapnik.Color('darkgreen'))

	# rules
	polygon_rule = mapnik.Rule()
	polygon_rule.symbols.append(polygon_symb)

	# styles
	polygon_style = mapnik.Style()
	polygon_style.rules.append(polygon_rule)

	# layers
	map_layer = mapnik.Layer('map_layer')
	map_layer.datasource = mapnik.Shapefile(file=WORLD_FILE)
	map_layer.styles.append('map_style')

	# map
	map = mapnik.Map(WIDTH, HEIGHT)
	map.background = mapnik.Color('steelblue')
	map.append_style('map_style', polygon_style)
	map.layers.append(map_layer)
	map.zoom_all()

	for x in range(NUM_TILES_X):
		for y in range(NUM_TILES_Y):
			tile_name = 'tile_%d_%d.png' % (x, y)
			mapnik.render_tile_to_file(map, x*TILE_SIZE, y*TILE_SIZE, TILE_SIZE,
				TILE_SIZE, tile_name, 'png')

	print('scale_denominator:%f, scale:%f' % (map.scale_denominator(), map.scale()))

if __name__ == '__main__':
	main()
