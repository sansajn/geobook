import sys, osgeo.ogr

WORLD_FILE = '../assets/world_borders_0.3/TM_WORLD_BORDERS-0.3.shp'
COUNTRY = 'Slovakia'

shapefile = osgeo.ogr.Open(WORLD_FILE)
layer = shapefile.GetLayer(0)

print('layer #0 features: %d' % (layer.GetFeatureCount(), ))

feature = layer.GetFeature(0)
print('feature #0 fields: %d' % (feature.GetFieldCount(), ))
for i in range(feature.GetFieldCount()):
	field = feature.GetFieldDefnRef(i)
	name = field.GetNameRef()
	
	type = field.GetType()
	if type == osgeo.ogr.OFTInteger:
		type_str = 'integer'
	elif type == osgeo.ogr.OFTIntegerList:
		type_str = 'integer list'
	elif type == osgeo.ogr.OFTReal:
		type_str = 'real'
	elif type == osgeo.ogr.OFTRealList:
		type_str = 'real list'
	elif type == osgeo.ogr.OFTString:
		type_str = 'string'
	elif type == osgeo.ogr.OFTStringList:
		type_str = 'string list'
	elif type == osgeo.ogr.OFTWideStringList:
		type_str = 'wide string list'
	elif type == osgeo.ogr.OFTBinary:
		type_str = 'binary'
	elif type == osgeo.ogr.OFTDate:
		type_str = 'date'
	elif type == osgeo.ogr.OFTTime:
		type_str = 'time'
	elif type == osgeo.ogr.OFTDateTime:
		type_str = 'date time'
	elif type == osgeo.ogr.OFTInteger64:
		type_str = 'integer 64bit'
	elif type == osgeo.ogr.OFTInteger64List:
		type_str = 'integer 64bit list'
	else:
		type_str = 'unknown'

	value = feature.GetField(name)

	print('  %s:%s <- %s' % (name, type_str, str(value)))

