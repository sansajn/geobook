#include <string>
#include <utility>
#include <memory>
#include <mapnik/map.hpp>
#include <mapnik/layer.hpp>
#include <mapnik/rule.hpp>
#include <mapnik/feature_type_style.hpp>
#include <mapnik/symbolizer.hpp>
#include <mapnik/text/placements/dummy.hpp>
#include <mapnik/text/formatting/text.hpp>
#include <mapnik/color_factory.hpp>
#include <mapnik/agg_renderer.hpp>
#include <mapnik/datasource_cache.hpp>
#include <mapnik/font_engine_freetype.hpp>
#include <mapnik/image_util.hpp>
#include <mapnik/save_map.hpp>

using std::make_shared;
using std::move;
using std::string;
using namespace mapnik;

int main(int argc, char * argv[])
{
	string const srs_lcc = "+proj=lcc +ellps=GRS80 +lat_0=49 +lon_0=-95 +lat+1=49 +lat_2=77 +datum=NAD83 +units=m +no_defs";
	string const srs_merc = "+proj=merc +a=6378137 +b=6378137 +lat_ts=0.0 +lon_0=0.0 +x_0=0.0 +y_0=0.0 +k=1.0 +units=m +nadgrids=@null +wktext +no_defs +over";

	datasource_cache::instance().register_datasources("/usr/lib/mapnik/3.0/input/");
	freetype_engine::register_font("/usr/share/fonts/truetype/dejavu/DejaVuSans.ttf");

	Map m{800, 600};
	m.set_background(parse_color("white"));
	m.set_srs(srs_merc);  // set map projection

	// styles

	feature_type_style provpoly_style;  // provinces (polygon)
	provpoly_style.reserve(2);
	{
		rule r;
		r.set_filter(parse_expression("[NAME_EN] = 'Ontario'"));
		{
			polygon_symbolizer poly_sym;
			put(poly_sym, keys::fill, color{250, 190, 183});
			r.append(move(poly_sym));
		}
		provpoly_style.add_rule(move(r));
	}
	{
		rule r;
		r.set_filter(parse_expression("[NOM_FR] = 'Québec'"));
		{
			polygon_symbolizer poly_sym;
			put(poly_sym, keys::fill, color{217, 235, 203});
			r.append(move(poly_sym));
		}
		provpoly_style.add_rule(move(r));
	}
	m.insert_style("provinces", move(provpoly_style));

	feature_type_style provlines_style;  // provinces (polyline)
	{
		rule r;
		{
			 line_symbolizer line_sym;
			 put(line_sym, keys::stroke,color{0, 0, 0});
			 put(line_sym, keys::stroke_width, 1.0);
			 dash_array dash;
			 dash.emplace_back(8, 4);
			 dash.emplace_back(2, 2);
			 dash.emplace_back(2, 2);
			 put(line_sym, keys::stroke_dasharray, dash);
			 r.append(move(line_sym));
		}
		provlines_style.add_rule(move(r));
	}
	m.insert_style("provlines", move(provlines_style));

	feature_type_style qcdrain_style;  // drainage
	{
		rule r;
		r.set_filter(parse_expression("[HYC] = 8"));
		{
			polygon_symbolizer poly_sym;
			put(poly_sym, keys::fill, color{153, 204, 255});
			r.append(move(poly_sym));
		}
		qcdrain_style.add_rule(move(r));
	}
	m.insert_style("drainage", move(qcdrain_style));

	feature_type_style roads34_style;  // roads 3 and 4 (the "grey" roads)
	{
		rule r;
		r.set_filter(parse_expression("[CLASS] = 3 or [CLASS] = 4"));
		{
			line_symbolizer line_sym;
			put(line_sym, keys::stroke, color{171, 158, 137});
			r.append(move(line_sym));
		}
		roads34_style.add_rule(move(r));
	}
	m.insert_style("smallroads", move(roads34_style));

	feature_type_style roads2_style_1;  // roads 2 (the thin yellow ones)
	{
		rule r;
		r.set_filter(parse_expression("[CLASS] = 2"));
		{
			line_symbolizer line_sym;
			put(line_sym, keys::stroke, color{171, 158, 137});
			put(line_sym, keys::stroke_width, 4.0);
			put(line_sym, keys::stroke_linecap, ROUND_CAP);
			put(line_sym, keys::stroke_linejoin, ROUND_JOIN);
			r.append(move(line_sym));
		}
		roads2_style_1.add_rule(move(r));
	}
	m.insert_style("road-border", move(roads2_style_1));

	feature_type_style roads2_style_2;
	{
		rule r;
		r.set_filter(parse_expression("[CLASS] = 2"));
		{
			line_symbolizer line_sym;
			put(line_sym, keys::stroke, color{255, 250, 115});
			put(line_sym, keys::stroke_width, 2.0);
			put(line_sym, keys::stroke_linecap, ROUND_CAP);
			put(line_sym, keys::stroke_linejoin, ROUND_JOIN);
			r.append(move(line_sym));
		}
		roads2_style_2.add_rule(move(r));
	}
	m.insert_style("road-fill", move(roads2_style_2));

	feature_type_style roads1_style_1;  // roads (the big orange ones, the highways)
	{
		rule r;
		r.set_filter(parse_expression("[CLASS] = 1"));
		{
			line_symbolizer line_sym;
			put(line_sym, keys::stroke, color{188, 149, 28});
			put(line_sym, keys::stroke_width, 7.0);
			put(line_sym, keys::stroke_linecap, ROUND_CAP);
			put(line_sym, keys::stroke_linejoin, ROUND_JOIN);
			r.append(move(line_sym));
		}
		roads1_style_1.add_rule(move(r));
	}
	m.insert_style("highway-border", move(roads1_style_1));

	feature_type_style roads1_style_2;
	{
		rule r;
		r.set_filter(parse_expression("[CLASS] = 1"));
		{
			line_symbolizer line_sym;
			put(line_sym, keys::stroke, color{242, 191, 36});
			put(line_sym, keys::stroke_width, 5.0);
			put(line_sym, keys::stroke_linecap, ROUND_CAP);
			put(line_sym, keys::stroke_linejoin, ROUND_JOIN);
			r.append(move(line_sym));
		}
		roads1_style_2.add_rule(move(r));
	}
	m.insert_style("highway-fill", move(roads1_style_2));

	// populate places
	feature_type_style popplaces_style;
	{
		rule r;
		{
			text_symbolizer text_sym;
			text_placements_ptr placement_finder = make_shared<text_placements_dummy>();
			placement_finder->defaults.format_defaults.face_name = "DejaVu Sans Book";
			placement_finder->defaults.format_defaults.text_size = 10.0;
			placement_finder->defaults.format_defaults.fill = color{0, 0, 0};
			placement_finder->defaults.format_defaults.halo_fill = color{255, 255, 200};
			placement_finder->defaults.format_defaults.halo_radius = 1.0;
			placement_finder->defaults.set_format_tree(
				make_shared<mapnik::formatting::text_node>(parse_expression("[GEONAME]")));
			put<text_placements_ptr>(text_sym, keys::text_placements_, placement_finder);
			r.append(move(text_sym));
		}
		popplaces_style.add_rule(move(r));
	}
	m.insert_style("popplaces", move(popplaces_style));

	// layers
	// provincial polygons
	{
		parameters p;
		p["type"] = "shape";
		p["file"] = "data/boundaries";
		p["encoding"] = "utf8";

		layer lyr{"Provinces"};
		lyr.set_datasource(datasource_cache::instance().create(p));
		lyr.add_style("provinces");
		lyr.set_srs(srs_lcc);

		m.add_layer(lyr);
	}

	// drainage
	{
		parameters p;
		p["type"] = "shape";
		p["file"] = "data/qcdrainage";

		layer lyr("Quebec Hydrography");
		lyr.set_datasource(datasource_cache::instance().create(p));
		lyr.set_srs(srs_lcc);
		lyr.add_style("drainage");

		m.add_layer(lyr);
	}

	// provincial boundaries
	{
		parameters p;
		p["type"] = "shape";
		p["file"] = "data/boundaries_l";

		layer lyr("Provincial borders");
		lyr.set_srs(srs_lcc);
		lyr.set_datasource(datasource_cache::instance().create(p));
		lyr.add_style("provlines");

		m.add_layer(lyr);
	}

	// roads
	{
		parameters p;
		p["type"] = "shape";
		p["file"] = "data/roads";

		layer lyr{"Roads"};
		lyr.set_srs(srs_lcc);
		lyr.set_datasource(datasource_cache::instance().create(p));
		lyr.add_style("smallroads");
		lyr.add_style("road-border");
		lyr.add_style("road-fill");
		lyr.add_style("highway-border");
		lyr.add_style("highway-fill");

		m.add_layer(lyr);
	}

	// popplaces
	{
		parameters p;
		p["type"] = "shape";
		p["file"] = "data/popplaces";
		p["encoding"] = "utf8";

		layer lyr{"Populated Places"};
		lyr.set_srs(srs_lcc);
		lyr.set_datasource(datasource_cache::instance().create(p));
		lyr.add_style("popplaces");

		m.add_layer(lyr);
	}

	m.zoom_to_box(box2d<double>(-8024477.28459, 5445190.38849, -7381388.20071, 5662941.44855));

	image_rgba8 buf(m.width(), m.height());
	agg_renderer<image_rgba8> ren{m, buf};
	ren.apply();

	save_to_file(buf, "map.png", "png");

	save_map(m, "map.xml");  // save map definition (data + style)

	return 0;
}
