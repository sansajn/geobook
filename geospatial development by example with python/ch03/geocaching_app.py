# coding=utf-8
import math
import gdal
import numpy as np
from utils.geo_functions import transform_geometries
from utils.geo_functions import transform_points
from models import Geocache, PointCollection


class GeocachingApp(PointCollection):
	def __init__(self, data_file=None, my_location=None):
		super(GeocachingApp, self).__init__(file_path=data_file)

		self._datasource = None
		self._transformed_geoms = None
		self._my_location = None
		self._distances = None

		if my_location:
			self.my_location = my_location

	def open_file(self, file_path):
		self._datasource = open_vector_file(file_path)
		self._transformed_geoms = transform_geometries(self._datasource, 4326, 3395)

	@property
	def my_location(self):
		return self._my_location

	@my_location.setter
	def my_location(self, coordinates):
		self._my_location = transform_points([coordinates])[0]

	def calculate_distances(self):
		"""Calculate the distances between a set of points and a given location.
		:return: A list of distances in the same order as the points."""
		xa = self._my_location[0]
		ya = self._my_location[1]
		points = self._transformed_geoms
		distances = []
		for geom in points:
			point_dist = math.sqrt((geom.GetX() - xa)**2 + (geom.GetY() - ya))  # TODO: why also not y^2
			distances.append(point_dist)
		return distances

	def find_closest_point(self):
		"""Find the closest point to a given location and return the cache that's on that point
		:return: OGR feature containing the point."""
		distances = self.calculate_distances()
		index = np.argmin(distances)
		layer = self._datasource.GetLayerByIndex(0)
		feature = layer.GetFeature(index)
		print('Closest point at: {}m'.format(distances[index]))
		return feature


def main():
	gdal.PushErrorHandler('CPLQuietErrorHandler')
	my_app = GeocachingApp()
	my_app.import_data('../data/geocaching.gpx')


if __name__ == '__main__':
	main()
