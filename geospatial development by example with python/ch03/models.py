import os
import gdal
from pprint import pprint
from utils.geo_functions import open_vector_file

class Geocache(object):
	"""Represents a single geocaching point."""
	def __init__(self, lat, lon, attributes=None):
		self.lat = lat
		self.lon = lon
		self.attributes = attributes

	@property
	def coordinates(self):
		return (self.lat, self.lon)


class PointCollection(object):
	def __init__(self, file_path=None):
		"""Represents a group of vector data."""
		self.data = []
		self.epsg = None

		if file_path:
			self.import_data(file_path)

	def import_data(self, file_path):
		features, metadata = open_vector_file(file_path)
		self._parse_data(features)
		self.epsg = metadata['epsg']
		print('File imported: {}'.format(file_path))

	def describe(self):
		print('SRS EPSG code: {}'.format(self.epsg))
		print('Number of features: {}'.format(len(self.data)))

	def __add__(self, other):
		self.data += other.data
		return self

	def _parse_data(self, features):
		for feature in features:
			geom = feature['geometry']['coordinates']
			attributes = feature['properties']
			cache_point = Geocache(geom[0], geom[1], attributes=attributes)
			self.data.append(cache_point)



if __name__ == '__main__':
	gdal.PushErrorHandler('CPLQuietErrorHandler')
	my_data = PointCollection('../data/geocaching.gpx')
	my_other_data = PointCollection('../data/geocaching.shp')
	merged_data = my_data + my_other_data
	merged_data.describe()
