# coding=utf-8

import cv2
import numpy as np
from tabulate import tabulate

class RasterData(object):
	def __init__(self, input_data, unchanged=True, shape=None):
		'''Represents a raster data in the form of an array.
		:param input_data: Raster files or Numpy array.
		:param unchanged: Set to true to keep the original format.
		:param shape: When using multiple input data, this param determines the shape of the composition.'''
		self.data = None
		self._stats = None
		if isinstance(input_data, list) or isinstance(input_data, tuple):
			self.combine_images(input_data, shape)
		else:
			self.import_data(input_data, unchanged)

	@property
	def stats(self):
		if self._stats is None:
			self._stats = self._calculate_stats()
		return self._stats

	def import_data(self, image, unchanged=True):
		'''Opens a raster file.
		:param image: Path of the raster file or np array.
		:param unchanged: Set to true to keep the original format.'''
		if isinstance(image, np.ndarray):
			self.data = image
			return image
		flags = cv2.CV_LOAD_IMAGE_UNCHANGED if unchanged else -1
		self.data = cv2.imread(image, flags=flags)

	def write_image(self, output_image):
		'''Write the data to the disk as an image.
		:param output_image: Path and name of the output image.'''
		cv2.imwrite(output_image, self.data)
		return self

	def combine_images(self, input_images, shape):
		'''Combine images in a mosaic.
		:param input_images: Path to the input images.
		:param shape: Shapeof the mosaic in columns and rows.'''
		if len(input_images) != shape[0] * shape[1]:
			raise ValueError("Number of images doesn't match the mosaic shape.")
		images = []
		for item in input_images:
			if isinstance(item, RasterData):
				images.append(item.data)
			else:
				images.append(RasterData(item).data)
		rows = []
		for row in range(shape[0]):
			start = row * shape[1]
			end = start + shape[1]
			rows.append(np.concatenate(images[start:end], axis=1))
		mosaic = np.concatenate(rows, axis=0)
		self.data = mosaic
		return self

	def adjust_values(self, img_range=None):
		'''Create a visualization of the data in the input_image by
		projecting a range of values into a grayscale image.
		:param img_range: specified range of values or None to use the
		range of the image (minimum and maximum).'''
		image = self.data
		if img_range:
			min = img_range[0]
			max = img_range[1]
		else:
			min = image.min()
			max = image.max()
		interval = max - min
		factor = 256.0 / interval
		output = image * factor
		self.data = output
		return self

	def crop_image(self, image_extent, bbox):
		'''Crops an image by a bounding box.
		bbox and image_extent format: (xmin, ymin, xmax, ymax).

		:param image_extent: The geographic extent of the image.
		:param bbox: The bounding box of the region of interest.'''
		input_image = self.data
		img_shape = input_image.shape
		img_geo_width = abs(image_extent[2] - image_extent[0])
		img_geo_height = abs(image_extent[3] - image_extent[1])

		# how much pixels are contained in one geographic unit.
		pixel_width = img_shape[1]/img_geo_width
		pixel_height = img_shape[0]/img_geo_height

		# index of the pixel to cut.
		x_min = abs(bbox[0] - image_extent[0]) * pixel_width
		x_max = abs(bbox[2] - image_extent[0]) * pixel_width
		y_min = abs(bbox[1] - image_extent[1]) * pixel_height
		y_max = abs(bbox[3] - image_extent[1]) * pixel_height

		output = input_image[y_min:y_max, x_min:x_max]
		self.data = output
		return self

	def create_hillshade(self, azimuth=90, angle_altitude=60):
		'''Creates a shaded relief image from a digital elevation model.
		:param azimuth: Simulated sun azimuth.
		:param angle_altitude: Sun altitude angle.'''
		input_image = self.data
		x, y = np.gradient(input_image)
		slope = np.pi / 2 - np.arctan(np.sqrt(x*x + y*y))
		aspect = np.arctan2(-x, y)
		az_rad = azimuth * np.pi / 180
		alt_rad = angle_altitude * np.pi / 180
		a = np.sin(alt_rad) * np.sin(slope)
		b = np.cos(alt_rad) * np.cos(slope) * np.cos(az_rad - aspect)
		output = 255 * (a+b+1)/2
		self.data = output
		return self

	def colorize(self, style):
		'''Produces an RGB image on a style containing limits and colors.
		:param style: A list of limits and colors.'''
		shape = self.data.shape
		limits = []
		colors = []
		# separate the limits and colors
		for item in style:
			limits.append(item[0])
			colors.append(self._convert_color(item[1]))
		colors = np.array(colors)
		# put each color in its limits.
		flat_array = self.data.flatten()
		di_array = np.digitize(flat_array, limits)
		di_array = di_array.reshape((shape[0], shape[1], 1))
		results = np.choose(di_array, colors)
		# convert from RGB to BGR
		results = np.asarray(results, dtype=np.uint8)
		results = cv2.cvtColor(results, cv2.COLOR_RGB2BGR)
		self.data = results
		return self

	def alpha_blend(self, raster_data, alpha=0.5):
		'''Blend this raster data with another one.
		:param raster_data: RasterData instance.
		:param alpha: Amount of transparency to apply.'''
		shade = cv2.cvtColor(raster_data.data, cv2.COLOR_GRAY2RGB)
		result = (1-alpha)*self.data + alpha*shade
		self.data = result
		return self

	def _convert_color(self, color_code):
		'''Converts the color notation.
		:param color_code: A string containing the color in hex or JavaScript notation.'''
		if color_code[0] == '#':
			result = (
				int(color_code[1:3], 16),
				int(color_code[3:5], 16),
				int(color_code[5:7], 16))
		elif color_code[:3] == 'rgb':
			result = map(int, color_code[4:-1].split(','))
		else:
			raise ValueError('Invalid color code.')
		return result

	def __repr__(self):
		stats = self._format_stats(self.stats)
		return 'Raster data - basic statistics.\n {}'.format(stats)

	def _check_data(self):
		"""Check if has data and if it's a Numpy array."""
		if self.data is None:
			raise ValueError('No data defined.')
		elif not isinstance(self.data, np.ndarray):
			raise TypeError('Wrong type of data.')

	def _calculate_stats(self):
		'''Calculate and return basic statistical information from the data.'''
		self._check_data()
		data = self.data
		stats = {
			'Minimum': data.min(),
			'Mean': data.mean(),
			'Maximum': data.max(),
			'Q1': np.percentile(data, 25),
			'Median': np.median(data),
			'Q3': np.percentile(data, 75),
			'Variance': data.var(),
			'Histogram': np.histogram(data)
		}
		return stats

	def _format_stats(self, stats, out_format='human'):
		'''Format the statistical data in a given output format.
		:param out_format: "human" or "csv"'''
		table = []
		for key, value in stats.iteritems():
			table.append([key, value])
		return tabulate(table)

if __name__ == '__main__':
	# shaded = RasterData('output/dem.tif')
	# shaded.adjust_values()\
	# 	.create_hillshade(10, 60)
	#
	# style = [
	# 	[700, '#f6eff7'],
	# 	[900, '#bdc9e1'],
	# 	[1100, '#67a9cf'],
	# 	[1300, '#11c9099'],
	# 	[1800, '#016c59']]
	#
	# classified = RasterData('output/dem.tif')
	# classified.adjust_values()\
	# 	.colorize(style)\
	# 	.alpha_blend(shaded)\
	# 	.write_image('output/color_shade.png')

	shaded = RasterData('output/shaded.png')
	classified = RasterData('output/classified.png')
	classified.alpha_blend(shaded)\
		.write_image('output/color_shade.png')
