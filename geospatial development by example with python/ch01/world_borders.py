import ogr

datasource = ogr.Open('../data/world_borders_simple.shp')
layer = datasource.GetLayerByIndex(0)
print('Number of features: {}'.format(layer.GetFeatureCount()))

# print number of features
feature_def = layer.GetLayerDefn()
for field_idx in range(feature_def.GetFieldCount()):
	field_def = feature_def.GetFieldDefn(field_idx)
	print('\t{}\t{}\t{}'.format(field_idx, field_def.GetTypeName(), field_def.GetName()))

# print country names
layer.ResetReading()
for feature in layer:
	print(feature.GetFieldAsString(4))
