# coding=utf-8

import os, itertools
import cv2 as cv
from osgeo import gdal, gdal_array
import numpy as np

def open_image(img_path):
	image = cv.imread(img_path)
	print(type(image))
	raw_input('press any key.')

def open_image_gdal(img_path):
	dataset = gdal.Open(img_path)
	cols = dataset.RasterXSize
	rows = dataset.RasterYSize
	print('Image dimensions: {} x {}px'.format(cols, rows))
	middle_col = int(cols/2)
	middle_row = int(rows/2)
	array = dataset.ReadAsArray(xoff=middle_col-50,
					yoff=middle_row-50, xsize=1000, ysize=1000)
	print(array.shape)
	greyscale_img = adjust_values(array)
	cv.imwrite('../output/landsat_chunk.jpg', greyscale_img)

def adjust_values(array, img_range=None):
	'''Projects a range of values into a grayscale image.
	:param array: N Numpy array containing the image data.
	:param img_range: specified range of values or None to
	use the range of the image (minimum and maximum).'''
	if img_range:
		min = img_range[0]
		max = img_range[1]
	else:
		min = array.min()
		max = array.max()
	interval = max - min
	factor = 256.0 / interval
	output = array * factor
	return output

def create_image_generator(dataset, crop_region=None):
	if not crop_region:
		cols = dataset.RasterXSize
		rows = dataset.RasterXSize
		xoff = 0
		yoff = 0
	else:
		xoff = crop_region[0]
		yoff = crop_region[1]
		cols = crop_region[2]
		rows = crop_region[3]
	for row_index in xrange(yoff, yoff+rows):
		yield dataset.ReadAsArray(xoff=xoff, yoff=row_index, xsize=cols, ysize=1)

def copy_image(src_image, dst_image):
	try:
		os.remove(dst_image)
	except OSError:
		pass

	src_dataset = gdal.Open(src_image)
	cols = src_dataset.RasterXSize
	rows = src_dataset.RasterYSize

	driver = gdal.GetDriverByName('GTiff')
	new_dataset = driver.Create(dst_image, cols, rows, eType=gdal.GDT_UInt16)
	gdal_array.CopyDatasetInfo(src_dataset, new_dataset)
	band = new_dataset.GetRasterBand(1)

	for index, img_row in enumerate(create_image_generator(src_dataset)):
		band.WriteArray(xoff=0, yoff=index, array=img_row)

def compose_band_path(base_path, base_name, band_number):
	return os.path.join(base_path, base_name) + str(band_number) + '.TIF'

def create_color_composition(bands, dst_image, crop_region=None):
	try:
		os.remove(dst_image)
	except OSError:
		pass

	datasets = map(gdal.Open, bands)
	img_iterators = list(
		itertools.imap(
			create_image_generator,
			datasets,
			itertools.repeat(crop_region)))

	if not crop_region:
		cols = datasets[0].RasterXSize
		rows = datasets[0].RasterYSize
	else:
		cols = crop_region[2]
		rows = crop_region[3]

	driver = gdal.GetDriverByName('GTiff')
	new_dataset = driver.Create(dst_image, cols, rows, eType=gdal.GDT_Byte, bands=3, options=['PHOTOMETRIC=RGB'])
	gdal_array.CopyDatasetInfo(datasets[0], new_dataset)

	rgb_bands = map(new_dataset.GetRasterBand, [1, 2, 3])
	for index, bands_rows in enumerate(itertools.izip(*img_iterators)):
		for band, row in zip(rgb_bands, bands_rows):
			row = adjust_values(row, [1000, 30000])
			band.WriteArray(xoff=0, yoff=index, array=row)


if __name__ == '__main__':
	base_path = '../data/landsat'
	base_name = 'LC80140282015270LGN00_B'
	#bands_numbers = [4, 3, 2]
	bands_numbers = [4, 5, 2]
	bands = itertools.imap(
		compose_band_path,
		itertools.repeat(base_path),
		itertools.repeat(base_name),
		bands_numbers)

	dst_image = '../output/color_composition.tif'
	create_color_composition(bands, dst_image, (1385, 5145, 1985, 1195))
