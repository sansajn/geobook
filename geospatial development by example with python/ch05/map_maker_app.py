# codeing=utf=8

import platform, tempfile
import mapnik, cv2
from map_maker.my_datasource import MapDatasource
from models import BoundaryCollection


class MapMakerApp(object):
	def __init__(self, output_image='map.png', style_file='map_maker/styles.xml', 
		map_size=(800, 600)):
		"""Application class."""

		self.output_image = output_image
		self.style_file = style_file
		self.map_size = map_size
		self._layers = {}

	def display_map(self):
		image = cv2.imread(self.output_image)
		cv2.imshow('image', image)
		cv2.waitKey(0)
		cv2.destroyAllWindows()

	def create_map(self):
		"""Create a map from XML file and writes it to an image."""
		map = mapnik.Map(*self.map_size)  # size unpack
		mapnik.load_map(map, self.style_file)
		layers = map.layers
		for name, layer in self._layers.iteritems():
			new_layer = mapnik.Layer(name)
			new_layer.datasource = layer['datasource']
			new_layer.styles.append(layer['style'])
			layers.append(new_layer)
		
		map.zoom_all()
		mapnik.render_to_file(map, self.output_image)

	def add_layer(self, geo_data, name, style='style1'):
		"""Add data to the map to be displayed in a layer with a given style.
		:param geo_data: a BaseGeoCollection subclass instance."""

		# this doesn't work in ubuntu 14.04, lets try windows workaround
		# datasource = mapnik.Python(factory='MapDatasource', mvt=geo_data)

		# TODO: this must be realy slow, get it working under linux
		print('windows workaround')
		temp_file, filename = tempfile.mkstemp(dir='temp')
		geo_data.export_geojson(filename)
		datasource = mapnik.GeoJSON(file=filename)

		layer = {
			'datasource': datasource,
			'data': geo_data,
			'style': style
		}

		self._layers[name] = layer


if __name__ == '__main__':
	world_borders = BoundaryCollection('../data/world_borders_simple.shp')
	countries = world_borders.filter('name', 'China') + world_borders.filter('name', 'India') + world_borders.filter('name', 'Japan')
	map_app = MapMakerApp('output/map.png')
	map_app.add_layer(countries, 'countries')
	map_app.create_map()
	map_app.display_map()
