# coding=utf-8

from __future__ import print_function
import json
import gdal
from shapely.geometry import Point, mapping
from shapely import wkb, wkt
from utils.geo_functions import open_vector_file
from utils.geo_functions import transform_geometry
from utils.geo_functions import convert_length_unit


class BaseGeoObject(object):
	"""Represents a collection of spatial data."""
	def __init__(self, geometry, attributes=None):
		self.geom = geometry
		self.attributes = attributes
		self.wm_geom = None

		self._attributes_lowercase = {}
		for key in self.attributes.keys():
			self._attributes_lowercase[key.lower()] = key

	def transformed_geom(self):
		"""Returns the geometry transformed into WorldMercator coordinate system."""
		if not self.wm_geom:
			geom = transform_geometry(self.geom.wkb)
			self.wm_geom = wkb.loads(geom) 
		return self.wm_geom

	@property
	def coordinates(self):
		raise NotImplementedError

	def get_attribute(self, attr_name, case_sensitive=False):
		if not case_sensitive:
			attr_name = attr_name.lower()
			attr_name = self._attributes_lowercase[attr_name]
		return self.attributes[attr_name]

	def export_geojson_feature(self):
		feature = {
			'type': 'Feature',
			'geometry': mapping(self.geom),
			'properties': self.attributes
		}
		return feature

	def __repr__(self):
		raise NotImplementedError


class Geocache(BaseGeoObject):
	"""Represents a single geocaching point."""
	def __init__(self, geometry, attributes=None):
		super(Geocache, self).__init__(geometry, attributes)

	def __repr__(self):
		name = self.attributes.get('name', 'Unnamed')
		return '{} {} - {}'.format(self.geom.x, self.geom.y, name)


class Boundary(BaseGeoObject):
	"""Represent a single geographics boundary."""
	def __repr__(self):
		return self.get_attribute('name')


class LineString(BaseGeoObject):
	"""Represents a single linestring."""
	def __repr__(self):
		unit = 'km'
		return '{} ({}{})'.format(
			self.get_attribute('name'), self.length(unit), unit)

	def length(self, unit='km'):
		return convert_length_unit(self.transformed_geom().length, unit)


class BaseGeoCollection(object):
	"""Represents a collection of spatial data."""
	def __init__(self, file_path=None):
		self.data = []
		self.epsg = None
		if file_path:
			self.import_data(file_path)

	def __add__(self, other):
		self.data += other.data
		return self

	def import_data(self, file_path):
		"""Opens an vector file compatible with OGR and parses the data."""
		features, metadata = open_vector_file(file_path)
		self._parse_data(features)
		self.epsg = metadata['epsg']
		print('File imported: {}'.format(file_path))

	def describe(self):
		print('SRS EPSG code: {}'.format(self.epsg))
		print('Number of features: {}'.format(len(self.data)))

	def get_by_name(self, name):
		for item in self.data:
			if item.get_attribute('name') == name:
				return item

		raise LookupError(
			'Object not found with the name: {}'.format(name))

	def filter_by_boundary(self, boundary):
		result = []
		for item in self.data:
			if item.geom.within(boundary.geom):
				result.append(item)
		return result

	def filter(self, attribute, value):
		result = self.__class__()
		for item in self.data:
			if item.get_attribute(attribute) == value:
				result.data.append(item)
		return result

	def export_geojson(self, file):
		features = [i.export_geojson_feature() for i in self.data]
		geojson = {
			'type': 'FeatureCollection',
			'features': features
		}

		with open(file, 'w') as out_file:
			json.dump(geojson, out_file, indent=2)

		print('File exported: {}'.format(file))

	def _parse_data(self):
		raise NotImplementedError


class PointCollection(BaseGeoCollection):
	"""Represents a collection of geocaching points."""
	def _parse_data(self, features):
		for feature in features:
			coords = feature['geometry']['coordinates']
			point = Point(float(coords[1]), float(coords[0]))
			attributes = feature['properties']
			cache_point = Geocache(point, attributes=attributes)
			self.data.append(cache_point)


class BoundaryCollection(BaseGeoCollection):
	"""Represents a collection of geographics bondaries."""
	def _parse_data(self, features):
		for feature in features:
			geom = feature['geometry']['coordinates']
			attributes = feature['properties']
			polygon = wkt.loads(geom)
			boundary = Boundary(geometry=polygon, attributes=attributes)
			self.data.append(boundary)


class LineStringCollection(BaseGeoCollection):
	"""Represents a collection of linestrings."""
	def _parse_data(self, features):
		for feature in features:
			geom = feature['geometry']['coordinates']
			attributes = feature['properties']
			line = wkt.loads(geom)
			linestring = LineString(geometry=line, attributes=attributes)
			self.data.append(linestring) 		


if __name__ == '__main__':
	gdal.PushErrorHandler('CPLQuietErrorHandler')
	points = PointCollection('../data/geocaching.gpx')
	points.export_geojson('output/data.json')
