import mapnik
from map_maker.datasource_impl import MapDatasource
from models import BoundaryCollection


class foo(object):
	def __init__(self, n):
		self.n = n

	def __repr__(self):
		return 'hello'

	def __str__(self):
		return 'hello!'


class MapMakerApp(object):
	def add_layer(self, geo_data, name):
		datasource = mapnik.Python(factory='MapDatasource', data=geo_data)
	

if __name__ == '__main__':
	world_borders = BoundaryCollection('../data/world_borders_simple.shp')
	map_app = MapMakerApp()
	#map_app.add_layer(world_borders, 'world')
	map_app.add_layer(foo(123), 'world')
