# coding=utf-8
from utils.geo_functions import open_vector_file
from utils.geo_functions import transform_geometries
from utils.geo_functions import transform_points
import numpy as np
import math


class GeocachingApp(object):
	def __init__(self, data_file=None, my_location=None):
		self._datasource = None
		self._transformed_geoms = None
		self._my_location = None
		self._distances = None

		if data_file:
			self.open_file(data_file)

		if my_location:
			self.my_location = my_location

	def open_file(self, file_path):
		self._datasource = open_vector_file(file_path)
		self._transformed_geoms = transform_geometries(self._datasource, 4326, 3395)

	@property
	def my_location(self):
		return self._my_location

	@my_location.setter
	def my_location(self, coordinates):
		self._my_location = transform_points([coordinates])[0]

	def calculate_distances(self):
		"""Calculate the distances between a set of points and a given location.
		:return: A list of distances in the same order as the points."""
		xa = self._my_location[0]
		ya = self._my_location[1]
		points = self._transformed_geoms
		distances = []
		for geom in points:
			point_dist = math.sqrt((geom.GetX() - xa)**2 + (geom.GetY() - ya))  # TODO: why also not y^2
			distances.append(point_dist)
		return distances

	def find_closest_point(self):
		"""Find the closest point to a given location and return the cache that's on that point
		:return: OGR feature containing the point."""
		distances = self.calculate_distances()
		index = np.argmin(distances)
		layer = self._datasource.GetLayerByIndex(0)
		feature = layer.GetFeature(index)
		print('Closest point at: {}m'.format(distances[index]))
		return feature


def main():
	my_app = GeocachingApp('../data/geocaching.gpx', [-73.0, 43.0])
	my_app.find_closest_point()


if __name__ == '__main__':
	main()
