#coding=utf-8
from __future__ import print_function
import ogr, osr

def open_vector_file(file_path):
	"""Opens a vector file compatible with OGR, gets the first layer and returns the OGR datasource."""
	datasource = ogr.Open(file_path)
	layer = datasource.GetLayerByIndex(0)
	print('Opening {}'.format(file_path))
	print('Number of features: {}'.format(layer.GetFeatureCount()))
	return datasource

def transform_geometries(datasource, src_epsg, dst_epsg):
	"""Transform the coordinates of all geometries in the first layer."""
	transformation = create_transform(src_epsg, dst_epsg)
	layer = datasource.GetLayerByIndex(0)

	geoms = []
	layer.ResetReading()
	for feature in layer:
		geom = feature.GetGeometryRef().Clone()
		geom.Transform(transformation)
		geoms.append(geom)

	return geoms

def transform_points(points, src_epsg=4326, dst_epsg=3395):
	"""Transform the coordinate reference system of a list of coordinates (a list of points)"""
	transform = create_transform(src_epsg, dst_epsg)
	points = transform.TransformPoints(points)
	return points

def create_transform(src_epsg, dst_epsg):
	""":return osr.CoordinateTransformation"""
	src_srs = osr.SpatialReference()
	src_srs.ImportFromEPSG(src_epsg)
	dst_srs = osr.SpatialReference()
	dst_srs.ImportFromEPSG(dst_epsg)
	return osr.CoordinateTransformation(src_srs, dst_srs)


if __name__ == '__main__':
	open_vector_file('../../data/geocaching.gpx')
