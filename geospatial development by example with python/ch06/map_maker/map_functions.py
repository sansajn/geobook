# coding=utf=8

import gdal
import mapnik
import cv2

def create_map(shapefile, gpx_file, style_file, output_image, size=(800,600)):
	"""Create a map from XML file and writes it to an image."""
	map = mapnik.Map(*size)  # size unpack
	mapnik.load_map(map, style_file)

	datasource = mapnik.Shapefile(file=shapefile)
	layers = map.layers

	world_datasource = mapnik.Shapefile(file=shapefile)
	world_layer = mapnik.Layer('world')
	world_layer.datasource = world_datasource
	world_layer.styles.append('styler')
	layers.append(world_layer)

	points_datasource = mapnik.Ogr(file=gpx_file, layer='waypoints')
	points_layer = mapnik.Layer('geocaching_points')
	points_layer.datasource = points_datasource
	points_layer.styles.append('style2')
	layers.append(points_layer)

	map.zoom_all()
	mapnik.render_to_file(map, output_image)


def display_map(image_file):
	image = cv2.imread(image_file)
	cv2.imshow('image', image)
	cv2.waitKey(0)
	cv2.destroyAllWindows()


if __name__ == '__main__':
	gdal.PushErrorHandler('CPLQuietErrorHandler')
	map_image = '../output/world3.png'
	create_map('../../data/world_borders_simple.shp', '../../data/geocaching.gpx', 
		'map_style.xml', map_image, size=(1024,500))
	display_map(map_image)
