# coding=utf-8

import mapnik

map = mapnik.Map(800, 600)
map.background = mapnik.Color('white')
style = mapnik.Style()
rule = mapnik.Rule()
polygon_symb = mapnik.PolygonSymbolizer(mapnik.Color('#f2eff9'))
rule.symbols.append(polygon_symb)
line_symb = mapnik.LineSymbolizer(mapnik.Color('rgb(50%,50%,50%)'), 0.1)
rule.symbols.append(line_symb)
style.rules.append(rule)
map.append_style('my style', style)
data = mapnik.Shapefile(file='../../data/world_borders_simple.shp')
layer = mapnik.Layer('world')
layer.datasource = data
layer.styles.append('my style')
map.layers.append(layer)
map.zoom_all()
mapnik.render_to_file(map, '../output/world.png', 'png')
