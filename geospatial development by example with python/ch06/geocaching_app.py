# coding=utf-8

import math
import gdal
import numpy as np
from utils.geo_functions import transform_points
from models import PointCollection, BoundaryCollection


class GeocachingApp(object):
	def __init__(self, geocaching_file=None, boundary_file=None, my_location=None):
		self.geocaching_data = PointCollection(geocaching_file)
		self.boundaries = BoundaryCollection(boundary_file)
		self._my_location = None

		if my_location:
			self.my_location = my_location

	@property
	def my_location(self):
		return self._my_location

	@my_location.setter
	def my_location(self, coordinates):
		self._my_location = transform_points([coordinates])[0]

	def calculate_distances(self):
		"""Calculate the distances between a set of points and a given location.
		:return: A list of distances in the same order as the points."""
		xa = self._my_location[0]
		ya = self._my_location[1]
		points = self._transformed_geoms
		distances = []
		for geom in points:
			point_dist = math.sqrt((geom.GetX() - xa)**2 + (geom.GetY() - ya))  # TODO: why also not y^2
			distances.append(point_dist)
		return distances

	def find_closest_point(self):
		"""Find the closest point to a given location and return the cache that's on that point
		:return: OGR feature containing the point."""
		distances = self.calculate_distances()
		index = np.argmin(distances)
		layer = self._datasource.GetLayerByIndex(0)
		feature = layer.GetFeature(index)
		print('Closest point at: {}m'.format(distances[index]))
		return feature

	def fileter_by_country(self, name):
		boundary = self.boundaries.get_by_name(name)
		return self.geocaching_data.filter_by_boundary(boundary)


if __name__ == '__main__':
	gdal.PushErrorHandler('CPLQuietErrorHandler')
	my_app = GeocachingApp('../data/geocaching.gpx', '../data/world_borders_simple.shp')
	result = my_app.fileter_by_country('United States')
	print(result)
