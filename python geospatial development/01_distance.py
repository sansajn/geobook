# vzdialenost dvoch bodov
import pyproj

lat1,lon1 = (37.82,-122.42)
lat2,lon2 = (37.80,-122.44)

geod = pyproj.Geod(ellps="WGS84")
angle1,angle2,distance = geod.inv(lon1,lat1, lon2,lat2)   # co su tie uhly

print 'Distance is %0.2f meters (angle1:%0.2f, angle2:%0.2f)' % (distance, angle1, angle2)
