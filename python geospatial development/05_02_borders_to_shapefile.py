import os, os.path, shutil
from osgeo import osr, ogr

# open the source shapefile
srcFile = ogr.Open('assets/TM_WORLD_BORDERS-0.3.shp')
srcLayer = srcFile.GetLayer(0)

# open the output shapefile
if os.path.exists('bounding-boxes'):
	shutil.rmtree('bounding-boxes')
os.mkdir('bounding-boxes')

spatialReference = osr.SpatialReference()
spatialReference.SetWellKnownGeogCS('WGS84')

driver = ogr.GetDriverByName('ESRI Shapefile')
dstPath = os.path.join('bounding-boxes', 'boudingBoxes.shp')
dstFile = driver.CreateDataSource(dstPath)
dstLayer = dstFile.CreateLayer('layer', spatialReference)

fieldDef = ogr.FieldDefn('COUNTRY', ogr.OFTString)
fieldDef.SetWidth(50)
dstLayer.CreateField(fieldDef)

fieldDef = ogr.FieldDefn('CODE', ogr.OFTString)
fieldDef.SetWidth(3)
dstLayer.CreateField(fieldDef)

# read the country features from the source shapefile
for i in range(srcLayer.GetFeatureCount()):
	feature = srcLayer.GetFeature(i)
	countryCode = feature.GetField('ISO3')
	countryName = feature.GetField('NAME')
	geometry = feature.GetGeometryRef()
	minLon, maxLon, minLat, maxLat = geometry.GetEnvelope()
	
	# save the bounding box as a feature in the output shapefile
	linearRing = ogr.Geometry(ogr.wkbLinearRing)
	linearRing.AddPoint(minLon, minLat)
	linearRing.AddPoint(maxLon, minLat)
	linearRing.AddPoint(maxLon, maxLat)
	linearRing.AddPoint(minLon, maxLat)
	linearRing.AddPoint(minLon, minLat)
	
	polygon = ogr.Geometry(ogr.wkbPolygon)
	polygon.AddGeometry(linearRing)
	
	feature = ogr.Feature(dstLayer.GetLayerDefn())
	feature.SetGeometry(polygon)
	feature.SetField('COUNTRY', countryName)
	feature.SetField('CODE', countryCode)
	dstLayer.CreateFeature(feature)
	feature.Destroy()
	
srcFile.Destroy()
dstFile.Destroy()

