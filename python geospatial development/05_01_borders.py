# read country borders
from osgeo import ogr

shapefile = ogr.Open('assets/TM_WORLD_BORDERS-0.3.shp')
layer = shapefile.GetLayer(0)

countries = []  # (code, name, minLat, maxLat, minLon, maxLon)

for i in range(layer.GetFeatureCount()):
	feature = layer.GetFeature(i)
	countryCode = feature.GetField('ISO3')
	countryName = feature.GetField('NAME')
	geometry = feature.GetGeometryRef()
	minLon, maxLon, minLat, maxLat = geometry.GetEnvelope()
	countries.append((countryName, countryCode, minLat, maxLat, minLon, maxLon))
	
countries.sort()

for name,code,minLat,maxLat,minLon,maxLon in countries:
	print('%s (%s) lat=%0.4f..%0.4f, lon=%0.4f..%0.4f' % (name, code, minLat, maxLat, minLon, maxLon))
