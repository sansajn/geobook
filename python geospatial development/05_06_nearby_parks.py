from osgeo import ogr
import shapely.geometry, shapely.wkt

MAX_DISTANCE = 0.1  # angular distance, approx 10 km

print('Loading urban areas ...')

urbanAreas = {}  # maps area name to Shapely polygon.

shapefile = ogr.Open('assets/cbsa/tl_2012_us_cbsa.shp')
layer = shapefile.GetLayer(0)

for i in range(layer.GetFeatureCount()):
	feature = layer.GetFeature(i)
	name = feature.GetField('NAME')
	geometry = feature.GetGeometryRef()
	shape = shapely.wkt.loads(geometry.ExportToWkt())
	dilatedShape = shape.buffer(MAX_DISTANCE)
	urbanAreas[name] = dilatedShape
	
print('Checking parks...')

f = file('assets/NationalFile_20161001.txt')
for line in f.readlines():
	chunks = line.rstrip().split('|')
	if len(chunks) < 4:
		print('line ignored', line)
		continue
		
	if chunks[2] == 'Park' and chunks[3] == 'CA':
		parkName = chunks[1]
		latitude = float(chunks[9])
		longitude = float(chunks[10])
		
		pt = shapely.geometry.Point(longitude, latitude)
		
		for urbanName, urbanArea in urbanAreas.items():
			if urbanArea.contains(pt):
				print parkName + ' is in or near ' + urbanName
				
f.close()

