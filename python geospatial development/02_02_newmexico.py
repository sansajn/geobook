import osgeo.ogr  # kniznica gdal

shapefile = osgeo.ogr.Open('assets/tl_2012_us_state.shp')

layer = shapefile.GetLayer(0)
feature = layer.GetFeature(2)  # new mexico

print 'Feature 2 has the fallowing attributes:'
print

attributes = feature.items();

for key,value in attributes.items():
	print '  %s = %s' % (key,value)

geometry = feature.GetGeometryRef()
geometryName = geometry.GetGeometryName()

print
print "Feature's geometry data consists of a %s" % geometryName

print 'done.'
