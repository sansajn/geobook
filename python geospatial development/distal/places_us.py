# reads from national file (US places name data)
import os, codecs, pyproj, osgeo.ogr
from pysqlite2 import dbapi2 as sqlite

NATIONAL_FILE = '../assets/NationalFile_20161001.txt'
DB_FILE = 'distal.db'
SPATIALITE_EXT_NAME = 'libspatialite.so.5'  # from libspatialite5 package

db = sqlite.connect(DB_FILE)
db.enable_load_extension(True)
db.execute('SELECT load_extension("%s")' % (SPATIALITE_EXT_NAME, ))  # TODO: replace ...
cursor = db.cursor()
cursor.execute('DELETE FROM places')

srcProj = pyproj.Proj(proj='longlat', ellps='GRS80', datum='NAD83')
dstProj = pyproj.Proj(proj='longlat', ellps='WGS84', datum='WGS84')

f = file(NATIONAL_FILE, 'r')
heading = f.readline()
num_inserted = 0
for line in f.readlines():
	parts = line.rstrip().split('|')
	if len(parts) < 11:
		print('ignore "%s" line' % line)
		continue

	featureName = parts[1]
	featureClass = parts[2]

	try:
		lat = float(parts[9])
		lon = float(parts[10])
	except ValueError:
		print('unable to parse "%s" or "%s"' % (parts[9], parts[10]))
		continue  # ignore line

	if featureClass == 'Populated Place':
		lon,lat = pyproj.transform(srcProj, dstProj, lon, lat)

		cursor.execute('''INSERT INTO places
			(name, position) VALUES 
			(?, MakePoint(?, ?, 4326))''',
			(featureName.decode("utf-8"), lon, lat))

		num_inserted += 1
		if num_inserted % 1000 == 0:
			db.commit()

f.close()
db.commit()

print('done!')
