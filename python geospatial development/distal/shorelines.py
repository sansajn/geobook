# reads data from GSHHS shapes
import os, sys
from pysqlite2 import dbapi2 as sqlite
import osgeo.ogr

DB_FILE = 'distal.db'
SPATIALITE_EXT_NAME = 'libspatialite.so.5'  # from libspatialite5 package

db = sqlite.connect(DB_FILE)
db.enable_load_extension(True)
db.execute('SELECT load_extension("%s")' % (SPATIALITE_EXT_NAME, ))
cursor = db.cursor()

cursor.execute('DELETE FROM shorelines')

for level in [1, 2, 3, 4]:
	srcFile = '../assets/GSHHS_shp/f/GSHHS_f_L%d.shp' % (level,)
	shapefile = osgeo.ogr.Open(srcFile)
	layer = shapefile.GetLayer(0)
	print("reading '%s', %d features ..." % (srcFile, layer.GetFeatureCount()))
	for i in range(layer.GetFeatureCount()):
		feature = layer.GetFeature(i)

		geometry = feature.GetGeometryRef()
		sys.stderr.write('%s with %d points\n' % (geometry.GetGeometryName(), geometry.GetPointCount()))

		wkt = feature.GetGeometryRef().ExportToWkt()

		# is polygon
		# how many points it has ?

		# TODO: MULTIPOLYGON not working with ST_PolygonFromText
		if wkt.startswith('MULTIPOLYGON'):
			sys.stderr.write("ignored %s feature %d, can't process MULTIPOLYGON geometry" % (srcFile, i))

		if not wkt.startswith('POLYGON'):
			sys.stderr.write("ignored '%s'" % (wkt[0:20], ))

		#sys.stderr.write('%s\n' % (wkt[0:20],))

		cursor.execute('''INSERT INTO shorelines 
			(level,outline) VALUES 
			(?, ST_PolygonFromText(?, 4326))''', 
			(level,wkt))
	db.commit()

print('done!')
