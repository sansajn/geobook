# create database tables
import os.path
from pysqlite2 import dbapi2 as sqlite

DB_FILE = 'distal.db'
SPATIALITE_EXT_NAME = 'libspatialite.so.5'  # from libspatialite5 package

if os.path.exists(DB_FILE):
	os.remove(DB_FILE)

db = sqlite.connect(DB_FILE)
db.enable_load_extension(True)
db.execute('SELECT load_extension("%s")' % (SPATIALITE_EXT_NAME, ))  # TODO: replace ...
cursor = db.cursor()
cursor.execute('SELECT InitSpatialMetaData()')  # initialize the spatialite meta-tables

# create database tables
cursor.execute('DROP TABLE IF EXISTS countries')
cursor.execute('''
	CREATE TABLE countries (
		id INTEGER PRIMARY KEY AUTOINCREMENT,
		name CHAR(255))
''')
cursor.execute('''
	SELECT AddGeometryColumn("countries", "outline", 4326, "POLYGON", 2)
''')
cursor.execute('''
	SELECT CreateSpatialIndex("countries", "outline")
''')

cursor.execute('''
	DROP TABLE IF EXISTS shorelines
''')
cursor.execute('''
	CREATE TABLE shorelines (
		id INTEGER PRIMARY KEY AUTOINCREMENT,
		level INTEGER)
''')
cursor.execute('''
	SELECT AddGeometryColumn("shorelines", "outline", 4326, "POLYGON", 2)
''')
cursor.execute('''
	SELECT CreateSpatialIndex("shorelines", "outline")
''')

cursor.execute('DROP TABLE IF EXISTS places')
cursor.execute('''
	CREATE TABLE places (
		id INTEGER PRIMARY KEY AUTOINCREMENT,
		name CHAR(255))
''')
cursor.execute('''
	SELECT AddGeometryColumn("places", "position", 4326, "POINT", 2)
''')
cursor.execute('''
	SELECT CreateSpatialIndex("places", "position")
''')

db.commit()

print('done!')
