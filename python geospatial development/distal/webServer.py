# create web-server at http://127.0.0.1:8000
import BaseHTTPServer, CGIHTTPServer

address = ('', 8000)
handler = CGIHTTPServer.CGIHTTPRequestHandler
handler.cgi_directories = ['/cgi-bin', 'elsewhere']
server = BaseHTTPServer.HTTPServer(address, handler)
print('listenning at http://127.0.0.1:%d' % (address[1],))
server.serve_forever()
