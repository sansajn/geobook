# reads geoname file
import os
from pysqlite2 import dbapi2 as sqlite

GEONAME_FILE = '../assets/Countries.txt'
DB_FILE = 'distal.db'
SPATIALITE_EXT_NAME = 'libspatialite.so.5'  # from libspatialite5 package

db = sqlite.connect(DB_FILE)
db.enable_load_extension(True)
db.execute('SELECT load_extension("%s")' % (SPATIALITE_EXT_NAME, ))  # TODO: replace ...
cursor = db.cursor()
print('database "%s" loaded' % DB_FILE)

f = file(GEONAME_FILE, 'r')
print('file "%s" opened' % GEONAME_FILE)

heading = f.readline()  # ignore heading
num_inserted = 0
num_line = 0
character_count = len(heading)

while True:
	line = f.readline()
	if len(line) == 0:  # eof
		break
	character_count += len(line)

	parts = line.rstrip().split('\t')
	if len(parts) < 23:
		print('ignore "%s" line' % line)
		continue

	try:
		lat = float(parts[3])
		lon = float(parts[4])
	except ValueError:
		print('unable to parse "%s" or "%s"' % (parts[3], parts[4]))
		continue

	featureClass = parts[9]
	featureDesignation = parts[10]
	nameType = parts[17]
	featureName = parts[22]

	if (featureClass == 'P' and nameType == 'N' and featureDesignation in ['PPL', 'PPLA', 'PPLC']):
		cursor.execute('''INSERT INTO places 
			(name,position) VALUES
			(?, MakePoint(?, ?, 4326))''',
			(featureName.decode("utf-8"), lon, lat))
		num_inserted += 1
		if num_inserted % 1000 == 0:
			db.commit()

	if num_line % 10000 == 0:
		print('line:%d, characters:%dM' % (num_line, character_count/1000000))
	num_line += 1

f.close()
db.commit()

print('done!')
