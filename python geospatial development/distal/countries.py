# reads countries from world shape
import os, sys
from pysqlite2 import dbapi2 as sqlite
import osgeo.ogr

DB_FILE = 'distal.db'
SPATIALITE_EXT_NAME = 'libspatialite.so.5'  # from libspatialite5 package

db = sqlite.connect(DB_FILE)
db.enable_load_extension(True)
db.execute('SELECT load_extension("%s")' % (SPATIALITE_EXT_NAME, ))  # TODO: replace ...
cursor = db.cursor()

cursor.execute('DELETE FROM countries')

srcFile = '../assets/TM_WORLD_BORDERS-0.3.shp'
shapefile = osgeo.ogr.Open(srcFile)
layer = shapefile.GetLayer(0)

for i in range(layer.GetFeatureCount()):
	feature = layer.GetFeature(i)
	name = feature.GetField('NAME').decode('Latin-1')
	wkt = feature.GetGeometryRef().ExportToWkt()

	# TODO: MULTIPOLYGON not working with ST_PolygonFromText
	if wkt.startswith('MULTIPOLYGON'):
		sys.stderr.write('ignored: ' + name + ", can't process multipolygon boundaries\n")
		continue

	cursor.execute('''
		INSERT INTO countries (name,outline)
		VALUES (?, ST_PolygonFromText(?, 4326))
		''', (name, wkt)
	)

db.commit()
