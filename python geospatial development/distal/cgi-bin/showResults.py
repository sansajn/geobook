#!/usr/bin/env python

# tu konvertujem uzivatelsky klik do mapy na (lat,lon) data

import sys, cgi, pyproj, shapely.wkt
import database, mapGenerator

MAX_WIDTH = 1000
MAX_HEIGHT = 800

database.open()

form = cgi.FieldStorage()

countryID = int(form['countryID'].value)
radius = int(form['radius'].value)
x = int(form['x'].value)
y = int(form['y'].value)
mapWidth = int(form['mapWidth'].value)
mapHeight = int(form['mapHeight'].value)

sys.stderr.write('clicked on (%d, %d) point\n' % (x,y))

details = database.get_country_details(countryID)
sys.stderr.write(details['name'] + '\n')
envelope = shapely.wkt.loads(details['bounds_wkt'])

minLon, minLat, maxLon, maxLat = envelope.bounds
minLon = minLon - 0.2
minLat = minLat - 0.2
maxLon = maxLon + 0.2
maxLat = maxLat + 0.2
sys.stderr.write('bounds:' + str(envelope.bounds) + '\n')

xFract = float(x)/float(mapWidth)
longitude = minLon + xFract * (maxLon - minLon)

yFract = float(y)/float(mapHeight)
latitude = minLat + (1 - yFract) * (maxLat - minLat)

sys.stderr.write('coordinates are (lat:%f, lon:%f)\n' % (latitude, longitude))

radius_in_meters = radius * 1000
#results = database.find_places_within(latitude, longitude, radius_in_meters)

#iLat = int(round(latitude))
#iLon = int(round(longitude))
#datasource = database.get_tiled_shoreline_datasource(iLat, iLon)
#datasource = database.get_shoreline_datasource()
datasource = database.get_country_datasource()

imgFile = mapGenerator.generateMap(
	datasource,
	minLon, minLat,
	maxLon, maxLat,
	600, 600,
	#points=results['places']
)

print('Content-Type: text/html; charset=UTF-8\n\n')
print('<html>')
print('<head><title>Search Results</title></head>')
print('<body>')
print('<b>' + details['name'] + '</b>')
print('<p>')
print('<img src="' + imgFile + '">')
print('</body>')
print('</html>')
