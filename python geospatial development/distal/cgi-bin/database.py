import os.path
import pyproj
from shapely.geometry import Polygon
import shapely.wkt

DB_FILE = 'distal.db'
SPATIALITE_EXT_NAME = 'libspatialite.so.5'  # from libspatialite5 package

def open():
	global _connection, _cursor
	from pysqlite2 import dbapi2 as sqlite
	_connection = sqlite.connect(DB_FILE)
	_connection.enable_load_extension(True)
	_connection.execute('SELECT load_extension("%s")' % (SPATIALITE_EXT_NAME, ))
	_cursor = _connection.cursor()

def list_countries():
	global _cursor
	results = []
	_cursor.execute('SELECT id,name FROM countries ORDER BY name')
	for id,name in _cursor:
		results.append((id, name.encode('utf-8')))
	return results

def get_country_details(country_id):
	global _cursor

	# SpatiaLite
	_cursor.execute('''
		SELECT name, ST_AsText(ST_Envelope(outline)) 
			FROM countries WHERE id=?''', 
		(country_id, )
	)

	row = _cursor.fetchone()
	if row != None:
		return {'name':row[0], 'bounds_wkt':row[1]}
	else:
		return None

def find_places_within(startLat, startLon, searchRadius):
	global _cursor

	geod = pyproj.Geod(ellps='WGS84')
	x, y, angle = geod.fwd(startLon, startLat, 0, searchRadius)
	maxLat = y
	x, y, angle = geod.fwd(startLon, startLat, 90, searchRadius)
	maxLon = x
	x, y, angle = geod.fwd(startLon, startLat, 180, searchRadius)
	minLat = y
	x, y, angle = geod.fwd(startLon, startLat, 270, searchRadius)
	minLon = x

	# DB_TYPE == 'SpatiaLite'
	_cursor.execute('''SELECT name, X(position), Y(position) 
		FROM places WHERE id in (
			SELECT pkid FROM idx_places_position 
				WHERE xmin >= ? AND xmax <= ? AND ymin >= ? and ymax <= ?
		)''', (minLon, maxLon, minLat, maxLat))

	places = []  # list of (lon, lat, name) tuples

	for row in _cursor:
		name, lon, lat = row
		angle1, angle2, distance = geod.inv(startLon, startLat, lon, lat)
		if distance > searchRadius:
			continue
		places.append([lon, lat, name])

	return {
		'places': places,
		'minLat': minLat,
		'minLon': minLon,
		'maxLat': maxLat,
		'maxLon': maxLon
	}

def get_shoreline_datasource():
	# DB_TYPE == 'SpatiaLite'
	return {
		'type': 'SQLite',
		'file': DB_FILE,
		'table': 'shorelines',
		'geometry_field': 'outline',
		'key_field': 'id'
	}

def get_country_datasource():
	# DB_TYPE == 'SpatiaLite'
	return {
		'type': 'SQLite',
		'file': DB_FILE,
		'table': 'countries',
		'geometry_field': 'outline',
		'key_field': 'id'
	}

def get_tiled_shoreline_datasource(iLat, iLon):
	# DB_TYPE == 'SpatiaLite'
	sql = "(SELECT outline FROM tiled_shorelines WHERE (intLat=%d) AND (intLong=%d))" % (
		iLat, iLon) + " AS shorelines"

	return {
		'type': "SQLite",
		'file': DB_FILE,
		'table': sql,
		'geometry_field': 'outline',
		'key_field': 'id'
	}
