import os.path
from osgeo import ogr
import pyproj

def getLineSegmentsFromGeometry(geometry):
	segments = []
	if geometry.GetPointCount() > 0:
		segment = []
		for i in range(geometry.GetPointCount()):
			segment.append(geometry.GetPoint_2D(i))
		segments.append(segment)
	for i in range(geometry.GetGeometryCount()):
		subGeometry = geometry.GetGeometryRef(i)
		segments.extend(
			getLineSegmentsFromGeometry(subGeometry))
	return segments
	
filename = os.path.join('common-border', 'border.shp')
shapefile = ogr.Open(filename)
layer = shapefile.GetLayer(0)
feature = layer.GetFeature(0)
geometry = feature.GetGeometryRef()
segments = getLineSegmentsFromGeometry(geometry)

geod = pyproj.Geod(ellps='WGS84')

totLength = 0.0
for segment in segments:
	for i in range(len(segment)-1):
		pt1 = segment[i]
		pt2 = segment[i+1]
		lon1,lat1 = pt1
		lon2,lat2 = pt2
		angle1, angle2, distance = geod.inv(lon1, lat1, lon2, lat2)
		totLength += distance

print('Total border length = %0.2f km' % (totLength/1000))
