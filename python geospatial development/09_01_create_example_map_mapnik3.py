# vyrenderuje mapu, mapnik3 kniznici
import mapnik

MIN_LAT = -35
MAX_LAT = +35
MIN_LON = -12
MAX_LON = +50

MAP_WIDTH = 700
MAP_HEIGHT = 800

# polygon style
polygonStyle = mapnik.Style()
rule = mapnik.Rule()
rule.filter = mapnik.Filter('[NAME] = "Angola"')
symbol = mapnik.PolygonSymbolizer()
symbol.fill = mapnik.Color('#604040')
rule.symbols.append(symbol)
polygonStyle.rules.append(rule)

rule = mapnik.Rule()
rule.filter = mapnik.Filter('[NAME] != "Angola"')
symbol = mapnik.PolygonSymbolizer()
symbol.fill = mapnik.Color('#406040')
rule.symbols.append(symbol)
polygonStyle.rules.append(rule)

rule = mapnik.Rule()
symbol = mapnik.LineSymbolizer()
symbol.stroke = mapnik.Color('#000000')
symbol.stroke_width = 0.1
rule.symbols.append(symbol)
polygonStyle.rules.append(rule)

# label style
labelStyle = mapnik.Style()
rule = mapnik.Rule()
symbol = mapnik.TextSymbolizer()
#symbol.face_name = 'Arial'
#symbol.text_size = 12
#symbol.fill = mapnik.Color('#000000')
#symbol = mapnik.TextSymbolizer(mapnik.Expression('[NAME]'), 'Arial', 12, mapnik.Color('#000000'))
rule.symbols.append(symbol)
labelStyle.rules.append(rule)

datasource = mapnik.Shapefile(file='assets/TM_WORLD_BORDERS-0.3.shp')

polygonLayer = mapnik.Layer('Polygons')  # refer to style by a name (rather then reference)
polygonLayer.datasource = datasource
polygonLayer.styles.append('PolygonStyle')

labelLayer = mapnik.Layer('Labels')
labelLayer.datasource = datasource
labelLayer.styles.append('LabelStyle')

# map object
map = mapnik.Map(MAP_WIDTH, MAP_HEIGHT, '+proj=longlat +datum=WGS84')
map.background = mapnik.Color('#8080a0')
map.append_style('PolygonStyle', polygonStyle)
#map.append_style('LabelStyle', labelStyle)
map.layers.append(polygonLayer)
#map.layers.append(labelLayer)
map.zoom_to_box(mapnik.Box2d(MIN_LON, MIN_LAT, MAX_LON, MAX_LAT))

# render map
mapnik.render_to_file(map, 'map_09_01.png')
