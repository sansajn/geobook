# vypocet priemernej vysky terenu
from osgeo import gdal, gdalconst
import struct

dataset = gdal.Open('assets/e10g')  # download from www.ngdc.noaa.gov/mgg/topo/gltiles.html (je potrbny aj hdr subor (www.ngdc.noaa.gov/mgg/topo/elev/esri/hdr/), GLOBE elavation dataset)
band = dataset.GetRasterBand(1)

fmt = '<' + ('h' * band.XSize)

totHeight = 0

print 'reading ...'
for y in range(band.YSize):
	scanline = band.ReadRaster(0, y, band.XSize, 1, band.XSize, 1, band.DataType)
	values = struct.unpack(fmt, scanline)
	for value in values:
		if value == -500:  # special height value for the sea -> ignore
			continue
		totHeight = totHeight + value;
	
average = totHeight / (band.XSize * band.YSize)
print 'Avarage height =', average

print 'done.'
