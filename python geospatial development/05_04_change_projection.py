import os, os.path, shutil
from osgeo import ogr, osr, gdal

# define the source destination projections, and a transformation object to convert from one to the other.

srcProjection = osr.SpatialReference()
srcProjection.SetUTM(17)

dstProjection = osr.SpatialReference()
dstProjection.SetWellKnownGeogCS('WGS84')  # lat/lon

transform = osr.CoordinateTransformation(srcProjection, dstProjection)

# open the source shapefile
srcFile = ogr.Open('assets/miami/miami.shp')
srcLayer = srcFile.GetLayer(0)

# create the dest shapefile
if os.path.exists('miami-reprojected'):
	shutil.rmtree('miami-reprojected')
os.mkdir('miami-reprojected')

driver = ogr.GetDriverByName('ESRI Shapefile')
dstPath = os.path.join('miami-reprojected', 'miami.shp')
dstFile = driver.CreateDataSource(dstPath)
dstLayer = dstFile.CreateLayer('layer', dstProjection)

# reproject each feature in turn
for i in range(srcLayer.GetFeatureCount()):
	feature = srcLayer.GetFeature(i)
	geometry = feature.GetGeometryRef()
	
	newGeometry = geometry.Clone()
	newGeometry.Transform(transform)
	
	feature = ogr.Feature(dstLayer.GetLayerDefn())
	feature.SetGeometry(newGeometry)
	dstLayer.CreateFeature(feature)
	feature.Destroy()

srcFile.Destroy()
dstFile.Destroy()

